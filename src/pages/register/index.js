import { Alert, Button, Card, CardActions, CardContent, TextField, Typography } from "@mui/material"
import { makeStyles } from "@mui/styles"
import { useState } from "react"
import { saveUser } from "../../service/register"



const useStyles = makeStyles(theme => ({
    registerWrapper: {
        display: 'flex',
        flex: 1,
        height: '100%',
        direction: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    registerCard: {
        width: '300px'
    }
}))


const RegisterPage = () => {
    const registerStyles = useStyles()
    const [error] = useState('')

    const [name, setName] = useState('')
    const [cpf, setCpf] = useState('')
    const [birthday, setBirthday] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')    

    const save = async () => {
        try {
            let user = {
                name,
                cpf,
                birthday,
                email,
                password
            }

            await saveUser(user)
            
            onclose(true)
        } catch (exception) {
            alert(exception.message)
        }
    }

    return(
        <div className={registerStyles.registerWrapper}>
            <Card className={registerStyles.registerCard}>
                <CardContent>
                    <Typography variant="h6" align="center">Word Travel</Typography>
                    <form>
                        <TextField label="Nome" variant="outlined" margin="normal" fullWidth
                            value={name} onChange={event => setName(event.target.value)} />

                        <TextField label="CPF" variant="outlined" margin="normal" fullWidth
                            value={cpf} onChange={event => setCpf(event.target.value)} />

                        <TextField label="Data de nascimento" variant="outlined" margin="normal" fullWidth
                            value={birthday} onChange={event => setBirthday(event.target.value)} />

                        <TextField label="Email" variant="outlined" margin="normal" fullWidth
                            value={email} onChange={event => setEmail(event.target.value)} />

                        <TextField label="Senha" variant="outlined" margin="normal" fullWidth
                            value={password} onChange={event => setPassword(event.target.value)} />
                    </form>

                    {error !== '' && <Alert severity="error">{error}</Alert>}                    
                </CardContent>
                <CardActions>
                    <Button color="primary" variant="contained" onClick={save} fullWidth>
                        Cadastre-se
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

export default RegisterPage