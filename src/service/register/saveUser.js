import api from "../api"
import { WS_USER } from "../config"

export const saveUser = async (user) => {
    try {
        await api.post(WS_USER, user)
    } catch(exception) {
        throw new Error('Cadastro não realizado')
    }
}