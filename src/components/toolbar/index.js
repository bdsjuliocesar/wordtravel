import { AppBar, Button, Toolbar, Typography } from '@mui/material'
import { useDispatch } from 'react-redux'
import { useHistory, useRouteMatch } from 'react-router'
import { Link } from 'react-router-dom'
import { logout } from '../../store/reducers/auth'

const AppToolbar = () => {
    const { url } = useRouteMatch()
    
    const dispatch = useDispatch()
    const history = useHistory()
    
    const exitApp = () => {
        dispatch(logout())
        history.push('/login')
    }

    const cadRoteiro = () => {
        history.push('/Roteiro')    
    }

    const cadUsuario = () => {
        history.push('/Usuario')    
    }

    return (
        <AppBar position='static'>
            <Toolbar>
                <Typography variant="h6">
                    Word Travel
                </Typography>
                
                <span style={{flexGrow: 1}} />
                <Button color="inherit" onClick={cadUsuario}>Cadastrar Usuário</Button>
                <Button color="inherit" onClick={cadRoteiro}>Cadastrar Roteiro</Button>
                <Button color="inherit" onClick={exitApp}>Sair</Button>

            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar